jQuery(document).ready(function() {
    jQuery('.search_icn .ua-icon.ua-icon-icon-111-search2 ').on('click',function(){
        jQuery('.search_frm').toggle();
    });

    jQuery('.fl-page-fixed-nav-search-wrap .fas.fa-search').on('click',function(){
        var top_ht = jQuery('.fl-page-header-fixed').height()+10;
        jQuery('.search_frm').toggle(); 
        jQuery('.search_frm').css({"position":"fixed", "top":top_ht});
    }); 

    
    jQuery(window).scroll(function(){
        if(jQuery('.fl-page-header-fixed').css('display') == 'none'){
            jQuery('.search_frm').hide();
            jQuery('.search_frm').css({"position":"absolute", "top":"100%"});
        }
    });
   
    jQuery(document).mouseup(function(e) {
        var container = jQuery(".search_frm, .search_icn .ua-icon.ua-icon-icon-111-search2 ");
    
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0 && jQuery('.search_frm').css('display') !=='none') 
        {
            jQuery('.search_frm').toggle();
        }
    });

});